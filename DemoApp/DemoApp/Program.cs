﻿using Squirrel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Task.Run(async () =>
            {
                try
                {
                    using (var mgr = new UpdateManager("https://bitbucket.org/NikhilPipal/demo/src/25fcfa8144f3b72b0ffad29193c386cf3a8d4dc4/DemoApp/Releases", "DemoApp"))
                    {
                        var updates = await mgr.CheckForUpdate();
                        if (updates.ReleasesToApply.Any())
                        {
                            var lastVersion = updates.ReleasesToApply.OrderBy(x => x.Version).Last();
                            await mgr.DownloadReleases(new[] { lastVersion });
                            await mgr.ApplyReleases(updates);
                            await mgr.UpdateApp();

                            Console.WriteLine("The application has been updated - please close and restart.");
                        }
                        else
                        {
                            Console.WriteLine("No Updates are available at this time.");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            });
            System.Threading.Thread.Sleep(20000);
        }
    }
}
